﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_017___delegates
{
    abstract class Employee
    {
        /// <summary>
        /// To stop working immediately, this must be implemented by derived class.
        /// </summary>
        public abstract void StopWorking();

        /// <summary>
        ///  Some jobs require a notice period before ending work safely.
        /// </summary>
        public virtual void StopWorking(int NoticePeriodInMinutes)
        {
            // Default is to rely on immediate stopping of work.
        }

        public virtual bool GiveNotice
        {
            get { return false; }
        }
    }

    class Roofer : Employee
    {
        public override void StopWorking()
        {
            Console.WriteLine("Roofer stops working.");
        }
    }

    class BrickLayer : Employee
    {
        public override void StopWorking()
        {
            Console.WriteLine("Brick layer stops working.");
        }
    }

    class Electrician : Employee
    {
        public override void StopWorking()
        {
            Console.WriteLine("Electrician stops working.");
        }
    }

    class CraneOperator : Employee
    {
        public override void StopWorking()
        {
            Console.WriteLine("Crane operator will stop working later.");
        }

        public override void StopWorking(int NoticePeriodInMinutes)
        {
            Console.WriteLine(String.Format("Crane operator stopped working after {0} minutes.", NoticePeriodInMinutes));
        }

        public override bool GiveNotice
        {
            get { return true; }
        }
    }

    class ConstructionSite
    {
        // Notice period for any workers needing an early warning is 10 minutes.
        const int NoticePeriodInMinutes = 10;

        // Define the delegate with return type of void and parameters,
        // which has no parameters for this example.
        delegate void EndShiftDelegate();

        public ConstructionSite()
        {
            _workers = new List<Employee>();
        }

        public void StartShift(Employee worker)
        {
            _workers.Add(worker);

            if (worker.GiveNotice)
            {
                // Use delegate adapter to convert the function taking a parameter to the delegate.
                _end_shift += delegate { worker.StopWorking(NoticePeriodInMinutes); };
            }
            else
            {
                // For each worker added, ensure they are added to the delegate that will
                // end their shift when EndShift is used later.
                _end_shift += worker.StopWorking;
            }
        }

        public void EndShift()
        {
            // Call all the added delegates, ending the shift for each worker added earlier.
            _end_shift();
        }

        // List of workers on their shift.
        private List<Employee> _workers;
        private EndShiftDelegate _end_shift;
    }

    class Program
    {
        static void Main(string[] args)
        {
            ConstructionSite site = new ConstructionSite();

            // Add all the workers who will start their shift.
            site.StartShift(new Roofer());
            site.StartShift(new BrickLayer());
            site.StartShift(new Electrician());
            site.StartShift(new CraneOperator());

            // End the shift, which will send all the workers home.
            site.EndShift();
        }
    }
}
