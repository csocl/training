﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lesson_015___indexers
{
    class Bits
    {
        /// <summary>
        ///  Construct an array of bits, all set to zero.
        /// </summary>
        /// <param name="size">Number of bits for array</param>
        public Bits(int size)
        {
            int byte_size = (size + 7) / 8;
            this._size = size;
            this._bytes = new byte[byte_size];
        }

        /// <summary>
        /// Get the size of bits in the array.
        /// </summary>
        public int Size
        {
            get { return _size; }
        }

        /// <summary>
        /// Implement [] on Bits class, allowing indexing of any bit within range.
        /// </summary>
        /// <param name="index">Bit position within the array to get or set.</param>
        /// <returns></returns>
        public bool this[int index]
        {
            get
            {
                if (index < _size)
                {
                    int byte_pos = index / 8;
                    return (_bytes[byte_pos] & (1 << (index % 8))) != 0;
                }
                return false;
            }

            set
            {
                if (index < _size)
                {
                    int byte_pos = index / 8;
                    if (value)
                        _bytes[byte_pos] |= (byte)(1 << (index % 8));
                    else
                        _bytes[byte_pos] &= (byte)(~(1 << (index % 8)));
                }
            }
        }

        /// <summary>
        /// Get the size of the array in bytes.
        /// </summary>
        public int GetByteSize()
        {
            return _bytes.Length;
        }

        /// <summary>
        /// Get the whole byte for the 8 bits from a byte position.
        /// </summary>
        public byte GetByteAt(int byte_pos)
        {
            return _bytes[byte_pos];
        }

        private byte[] _bytes;
        private int _size; // Size in bits.
    }

    class Program
    {
        static void Main(string[] args)
        {
            int size = 17;
            Bits b = new Bits(size);

            // Test bits that are set or not set.
            b[0] = true;
            b[7] = true;
            b[8] = true;
            b[9] = true;
            b[14] = true;
            b[16] = true;
            b[17] = true;
            Debug.Assert(b.Size == size);
            Debug.Assert(b.GetByteSize() == (size + 7) / 8);
            Debug.Assert(b.GetByteAt(0) == 129);
            Debug.Assert(b.GetByteAt(1) == 67);
            Debug.Assert(b.GetByteAt(2) == 1);
            Debug.Assert(b[0] == true);
            Debug.Assert(b[1] == false);
            Debug.Assert(b[2] == false);
            Debug.Assert(b[3] == false);
            Debug.Assert(b[4] == false);
            Debug.Assert(b[5] == false);
            Debug.Assert(b[6] == false);
            Debug.Assert(b[7] == true);
            Debug.Assert(b[8] == true);
            Debug.Assert(b[9] == true);
            Debug.Assert(b[10] == false);
            Debug.Assert(b[11] == false);
            Debug.Assert(b[12] == false);
            Debug.Assert(b[13] == false);
            Debug.Assert(b[14] == true);
            Debug.Assert(b[15] == false);
            Debug.Assert(b[16] == true);
            Debug.Assert(b[17] == false);
            Debug.Assert(b[18] == false);

            // Test changing set bits to unset, or vice versa.
            b[1] = true;
            b[7] = false;
            b[8] = true;
            b[9] = false;
            b[13] = true;
            b[14] = true;
            b[16] = false;
            b[17] = true;
            Debug.Assert(b.Size == size);
            Debug.Assert(b.GetByteSize() == (size + 7) / 8);
            Debug.Assert(b.GetByteAt(0) == 3);
            Debug.Assert(b.GetByteAt(1) == 97);
            Debug.Assert(b.GetByteAt(2) == 0);
            Debug.Assert(b[0] == true);
            Debug.Assert(b[1] == true);
            Debug.Assert(b[2] == false);
            Debug.Assert(b[3] == false);
            Debug.Assert(b[4] == false);
            Debug.Assert(b[5] == false);
            Debug.Assert(b[6] == false);
            Debug.Assert(b[7] == false);
            Debug.Assert(b[8] == true);
            Debug.Assert(b[9] == false);
            Debug.Assert(b[10] == false);
            Debug.Assert(b[11] == false);
            Debug.Assert(b[12] == false);
            Debug.Assert(b[13] == true);
            Debug.Assert(b[14] == true);
            Debug.Assert(b[15] == false);
            Debug.Assert(b[16] == false);
            Debug.Assert(b[17] == false);
            Debug.Assert(b[18] == false);
        }
    }
}
