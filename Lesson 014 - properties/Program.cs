﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_014___properties
{
    interface IPerson
    {
        // name and dob properties need to be implemented by derived class.
        String Name { get; set; }
        DateTime DateOfBirth { get; set; }
    }

    class Person : IPerson
    {
        public Person(String name, DateTime dob)
        {
            text_name = name;
            text_dob = dob;
        }

        // A cleaner alternative to writing your own GetName and SetName functions.
        // NOTE: The property could be defined as virtual, if you wanted derived classes
        //       to override name. E.g. public virtual String name.
        public String Name
        {
            get { return text_name; }
            set { text_name = value; }
        }

        // Get or set the date of birth.
        public DateTime DateOfBirth
        {
            get { return text_dob; }
            set { text_dob = value; }
        }

        // Read-only property to get the age in years.
        // NOTE: This won't be accurate, as it's an approximation of days/365.
        public int Age
        {
            get { return (DateTime.Now.Date - text_dob.Date).Days / 365; }
        }

        private String _name;
        private DateTime _dob;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Bob", new DateTime(1981, 01, 01));

            String dob = p.DateOfBirth.Date.Day.ToString() + "/" +
                         p.DateOfBirth.Date.Month.ToString() + "/" +
                         p.DateOfBirth.Date.Year.ToString();

            Console.WriteLine(p.Name + " born " + dob + " (age " + p.Age + ")");
        }
    }
}
