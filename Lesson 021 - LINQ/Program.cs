﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_021___LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            // Language-Integrated-Query (LINQ) uses queries to examine the contents of enumerable collections.

            var customers = new[]
            {
                new { ID = 1, Name = "Bob" },
                new { ID = 2, Name = "Jane" },
                new { ID = 3, Name = "John" },
                new { ID = 4, Name = "Stacy" }
            };

            var addresses = new[]
            {
                new { ID = 1, Address = "15 Bond Street" },
                new { ID = 2, Address = "8 High Street" },
                new { ID = 3, Address = "44 Lower Avenue" },
                new { ID = 4, Address = "60 Long Drive" },
            };

            // Example of selecting the name from the customers array.
            Console.WriteLine("All names");
            var names = from customer in customers select customer.Name;
            foreach (var name in names)
                Console.WriteLine(name);

            // Example of selecting all names with Select function and lambda.
            Console.WriteLine("\nAll names");
            var names2 = customers.Select(customer => customer.Name);
            foreach (var name in names2)
                Console.WriteLine(name);

            // Example of selecting the names where the ID > 2.
            Console.WriteLine("\nNames where ID > 2");
            var names3 = from customer in customers where customer.ID > 2 select customer.Name;
            foreach (var name in names3)
                Console.WriteLine(name);

            // Example of selecting customers where ID > 2.
            Console.WriteLine("\nNames where ID > 2");
            var match_customers = customers.Where(customer => customer.ID > 2);
            foreach (var customer in match_customers)
                Console.WriteLine(customer.Name);

            // Match the address for each customer and display it.
            foreach (var customer in customers)
            {
                var matched_addresses = addresses.Where(address => address.ID == customer.ID);
                foreach (var matched_address in matched_addresses)
                    Console.WriteLine($"{customer.Name} lives at {matched_address.Address}");
            }
        }
    }
}
