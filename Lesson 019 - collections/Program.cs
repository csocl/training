﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_019___collections
{
    class Program
    {
        // Some collections are inherited from ICollection,
        // and might have IEnumerable interface to allow iteration.
        static void Printcollection<T>(IEnumerable<T> values)
        {
            foreach (var value in values)
                Console.Write($"{value} ");
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            //
            // The List is a collection optimized for indexing,
            // with additional methods not available on an [] array, such as Sort.
            //
            List<int> ints_list = new List<int>();

            // Add 3 values to the end of the list.
            ints_list.Add(3);
            ints_list.Add(4);
            ints_list.Add(2);

            // Provide some examples of accessing the list.
            Console.WriteLine("List example:");
            Console.WriteLine($"indexed: {ints_list[0]} {ints_list[1]} {ints_list[2]}");
            Printcollection(ints_list);
            ints_list.Sort();
            Printcollection(ints_list);

            //
            // LinkedList is a double-ended ordered list, optimized for fast insertion and removal.
            //
            LinkedList<int> ints_linked_list = new LinkedList<int>();
            ints_linked_list.AddFirst(4);
            ints_linked_list.AddFirst(5);
            ints_linked_list.AddLast(3);
            LinkedListNode<int> last = ints_linked_list.Find(3);
            ints_linked_list.AddBefore(last, 2);

            Console.WriteLine("\nLinkedList example:");
            Printcollection(ints_linked_list);

            //
            // Queue provides a first in first out collection.
            //
            Queue<int> ints_queue = new Queue<int>();
            ints_queue.Enqueue(4);
            ints_queue.Enqueue(5);
            ints_queue.Enqueue(3);

            Console.WriteLine("\nQueue example:");
            foreach (var i in ints_queue)
                Console.Write($"{i} ");
            Console.WriteLine();

            // Remove each item and display the value.
            while (ints_queue.Count > 0)
                Console.Write($"{ints_queue.Dequeue()} ");
            Console.WriteLine();

            //
            // Stack provides a first in last out collection.
            //
            Stack<int> ints_stack = new Stack<int>();
            ints_stack.Push(4);
            ints_stack.Push(5);
            ints_stack.Push(3);

            Console.WriteLine("\nStack example:");
            foreach (var i in ints_stack)
                Console.Write($"{i} ");
            Console.WriteLine();

            // Remove each item and display the value.
            while (ints_stack.Count > 0)
                Console.Write($"{ints_stack.Pop()} ");
            Console.WriteLine();

            //
            // Dictionary provides a key and value pair for inserting a relationship
            // between the key and value, then finding the value based on the key later.
            //
            Dictionary<int, string> lookup = new Dictionary<int, string>();
            lookup.Add(1, "One");
            lookup.Add(2, "Two");
            lookup.Add(3, "Three");

            Console.WriteLine("\nDictionary example:");
            Printcollection(lookup);
            Console.WriteLine($"{lookup[1]} {lookup[2]} {lookup[3]}");

            bool four_found = false;
            string four = "";
            try
            {
                four = lookup[4];
                four_found = true;
            }
            catch(KeyNotFoundException)
            {
                Console.WriteLine("Four not found");
            }
            finally
            {
                if (four_found)
                    Console.WriteLine($"{four}");
            }
        }
    }
}
