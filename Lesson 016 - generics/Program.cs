﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lesson_016___generics
{
    /// <summary>
    /// Node class providing 0..n nodes for each node of the tree.
    /// T needs to be comparable for the Find function to work.
    /// </summary>
    /// <typeparam name="T">Any type of data stored for the tree.</typeparam>
    class Node<T> where T : IComparable
    {
        /// <summary>
        /// Construct this node with a value.
        /// </summary>
        public Node(T value, Node<T> parent = null)
        {
            _parent = parent; // When null this is the head node.
            _children = null;
            _value = value;
        }

        /// <summary>
        /// Add a child not with a value to the end of an array of nodes for this node.
        /// </summary>
        /// <returns>The newly added node is returned.</returns>
        public Node<T> AddChild(T value)
        {
            if (_children != null)
            {
                int len = _children.Length;
                Node<T>[] new_children = new Node<T>[len + 1];
                for (int i = 0; i < len; ++i)
                    new_children[i] = _children[i];
                new_children[len] = new Node<T>(value, this);
                _children = new_children;
                return _children[len];
            }

            _children = new Node<T>[1];
            _children[0] = new Node<T>(value, this);
            return _children[0];
        }

        /// <summary>
        /// Find the top level node.
        /// </summary>
        public Node<T> Head
        {
            get
            {
                Node<T> top = _parent;
                while (top != null)
                    top = top._parent;
                return top;
            }
        }

        /// <summary>
        /// Get the value for this node.
        /// </summary>
        public T Value
        {
            get { return _value; }
        }

        /// <summary>
        /// Return true when this node is the head node.
        /// </summary>
        public bool IsHead
        {
            get { return _parent == null; }
        }

        /// <summary>
        /// Return true when the node has a parent.
        /// </summary>
        public bool HasParent
        {
            get { return _parent != null; }
        }

        /// <summary>
        /// Return true when there are 1 or more children for this node.
        /// </summary>
        public bool HasChildren
        {
            get { return _children != null; }
        }

        /// <summary>
        /// Count all the children nodes recursively.
        /// </summary>
        public int Count
        {
            get
            {
                int count = 1;

                if (HasChildren)
                    foreach (Node<T> n in _children)
                        count += n.Count;

                return count;
            }
        }

        /// <summary>
        /// Get the number of immediate children in the node.
        /// </summary>
        public int Length
        {
            get { return HasChildren ? _children.Length : 0; }
        }

        /// <summary>
        /// Find a node matching the value.
        /// </summary>
        public Node<T> Find(T value)
        {
            if (Value.CompareTo(value) == 0)
                return this;
            if (_children != null)
            {
                foreach (Node<T> n in _children)
                {
                    Node<T> found_node = n.Find(value);
                    if (found_node != null)
                        return found_node;
                }
            }
            return null;
        }

        /// <summary>
        /// Print the values for all the nodes.
        /// </summary>
        public void Print()
        {
            Console.WriteLine(Value);
            if (HasChildren)
                foreach (Node<T> n in _children)
                    n.Print();
        }

        private T _value;
        private Node<T> _parent;
        private Node<T>[] _children;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Node<int> head = new Node<int>(1);
            Node<int> child = head.AddChild(11);
            child.AddChild(101);
            child.AddChild(102);

            child = head.AddChild(12);
            child.AddChild(201);
            child.AddChild(202);
            child.AddChild(203);

            Debug.Assert(head.Find(1) != null);
            Debug.Assert(head.Find(11) != null);
            Debug.Assert(head.Find(12) != null);
            Debug.Assert(head.Find(101) != null);
            Debug.Assert(head.Find(102) != null);
            Debug.Assert(head.Find(201) != null);
            Debug.Assert(head.Find(202) != null);
            Debug.Assert(head.Find(203) != null);

            Console.WriteLine("Count = " + head.Count);
            head.Print();
        }
    }
}
