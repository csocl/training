﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_018___events
{
    // Weather conditions available for weather events.
    enum WeatherState
    {
        Sunny = 0,
        Cloudy = 1,
        Hailing = 2,
        Raining = 3,
        Snowing = 4
    }

    // Define properties for weather condition event.
    class WeatherConditionEventArgs : EventArgs
    {
        public WeatherState WeatherCondition { get; set; }
        public DateTime CurrentDateTime { get; set; }
    }

    // Keep running the weather monitor until the rain starts pouring.
    class WeatherMonitoringStation
    {
        public event EventHandler<WeatherConditionEventArgs> WeatherConditionChange;

        public void StartMonitoring()
        {
            var rand = new Random();
            int rand_num = 0;

            try
            {
                Console.WriteLine("Starting monitoring");

                // Create the weather condition arguments to always start as Sunny,
                // so the weather loop is always entered.
                var weather_condition_args = new WeatherConditionEventArgs();
                weather_condition_args.WeatherCondition = WeatherState.Sunny;
                weather_condition_args.CurrentDateTime = DateTime.Now;

                // Keep showing weather condition until it rains.
                while (weather_condition_args.WeatherCondition != WeatherState.Raining)
                {
                    // Invoke the weather change event.
                    OnWeatherConditionChange(weather_condition_args);

                    // Randomly assign a weather condition.
                    rand_num = rand.Next((int)WeatherState.Sunny, (int)WeatherState.Snowing);
                    weather_condition_args.WeatherCondition = (WeatherState)rand_num;
                    weather_condition_args.CurrentDateTime = DateTime.Now;
                }

                // Invoke the final weather change event for raining.
                OnWeatherConditionChange(weather_condition_args);
            }

            catch (Exception)
            {
                Console.WriteLine("Failed to monitor weather");
            }
        }

        protected virtual void OnWeatherConditionChange(WeatherConditionEventArgs args)
        {
            // Call the delegate when it's not null.
            WeatherConditionChange?.Invoke(this, args);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Add a 
            WeatherMonitoringStation weather_station = new WeatherMonitoringStation();
            weather_station.WeatherConditionChange += WeatherMonitoringStationEvent;
            weather_station.StartMonitoring();
        }

        public static void WeatherMonitoringStationEvent(object sender, WeatherConditionEventArgs args)
        {
            String message = args.WeatherCondition == WeatherState.Raining
                ? "It " + args.WeatherCondition.ToString() + " at " + args.CurrentDateTime.ToString()
                : "Weather is " + args.WeatherCondition.ToString();
            Console.WriteLine(message);
        }
    }
}
