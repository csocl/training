﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_020___lambda_expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Define a lambda that does not return a value, and takes one string argument,
            // with the use of Action.
            Action<string> PrintMessage = message =>
            {
                Console.WriteLine(message);
            };

            PrintMessage("Hello");

            // Define a lambda that returns a value, and takes two arguments
            // with the use of Func.
            Func<int, int, int> Multiply = (x, y) => x * y;
            Console.WriteLine(Multiply(3, 4));

            // Define a lambda returning a value, which is implemented using a body.
            Func<int, int, int> Sum = (x, y) =>
            {
                return x + y;
            };

            Console.WriteLine(Sum(3, 4));
        }
    }
}
